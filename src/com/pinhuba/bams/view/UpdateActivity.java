package com.pinhuba.bams.view;

import com.pinhuba.bams.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class UpdateActivity extends Activity {
	private TextView name;
	private TextView desc;
	private Button nowUpdateBtn;
	private Button laterUpdateBtn;

	private String versionName;
	private String versionDescription;
	private String versionURL;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去标题
		setContentView(R.layout.update);
		initView();
		initData();
		setListener();
	}

	private void initView() {
		name = (TextView) findViewById(R.id.versionname_tv);
		desc = (TextView) findViewById(R.id.version_description_tv);
		nowUpdateBtn = (Button) findViewById(R.id.now_update_bt);
		laterUpdateBtn = (Button) findViewById(R.id.later_update_bt);
	}

	private void initData() {
		versionName = getIntent().getStringExtra("versionName");
		versionDescription = getIntent().getStringExtra("versionDescription");
		versionURL = getIntent().getStringExtra("versionURL");

		name.setText(versionName);
		desc.setText(versionDescription.replace(";", ";\n"));
	}

	private void setListener() {
		nowUpdateBtn.setOnClickListener(nowUpdateListener);
		laterUpdateBtn.setOnClickListener(laterUpdateListener);
	}

	private OnClickListener nowUpdateListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent it = new Intent(UpdateActivity.this, DownloadActivity.class);
			it.putExtra("versionName", versionName);
			it.putExtra("versionURL", versionURL);
			startActivity(it);
			UpdateActivity.this.finish();
		}
	};

	private OnClickListener laterUpdateListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			UpdateActivity.this.finish();
		}
	};

}
