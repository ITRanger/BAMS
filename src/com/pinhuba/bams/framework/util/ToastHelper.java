package com.pinhuba.bams.framework.util;

import android.widget.Toast;

public class ToastHelper {
	private static Toast toast;

	/**
	 * Using Toast way pop-up messages (display 3 seconds)
	 * 
	 * @Title: showLongMsg
	 * @param msg_int
	 */
	public static void showLongMsg(int msg_int) {
		if (toast == null) {
			toast = Toast.makeText(MyApplication.getAppContext(), msg_int, Toast.LENGTH_LONG);
		}
		// 版本高于4.0时候，不执行toast.cancel
		if (Version.getVersionSDK() < 14) {
			toast.cancel();

		}
		// toast.cancel();
		toast.setText(msg_int);
		toast.show();
	}

	/**
	 * Using Toast way pop-up messages (display 3 seconds)
	 * 
	 * @Title: showLongMsg
	 * @param msg
	 */
	public static void showLongMsg(String msg) {
		if (toast == null) {
			toast = Toast.makeText(MyApplication.getAppContext(), msg, Toast.LENGTH_LONG);
		}
		// 版本高于4.0时候，不执行toast.cancel
		if (Version.getVersionSDK() < 14) {
			toast.cancel();

		}
		// toast.cancel();
		toast.setText(msg);
		toast.show();
	}

	/**
	 * Using Toast way pop-up messages (display 2 seconds)
	 * 
	 * @Title: showShortMsg
	 * @param msg
	 */
	public static void showShortMsg(String msg) {
		if (toast == null) {
			toast = Toast.makeText(MyApplication.getAppContext(), msg, Toast.LENGTH_SHORT);
		}
		// 版本高于4.0时候，不执行toast.cancel
		if (Version.getVersionSDK() < 14) {
			toast.cancel();

		}
		// toast.cancel();
		toast.setText(msg);
		toast.show();
	}

	/**
	 * Using Toast way pop-up messages (display 2 seconds)
	 * 
	 * @Title: showShortMsg
	 * @param msg_int
	 */
	public static void showShortMsg(int msg_int) {
		if (toast == null) {
			Loger.d("The toast is null");
			toast = Toast.makeText(MyApplication.getAppContext(), msg_int, Toast.LENGTH_SHORT);
		}
		// 版本高于4.0时候，不执行toast.cancel
		if (Version.getVersionSDK() < 14) {
			toast.cancel();

		}
		// toast.cancel();
		toast.setText(msg_int);
		toast.show();
	}

	/**
	 * @see {@link android.widget.Toast#cancel()}
	 */
	public static void cancel() {
		if (toast != null) {
			toast.cancel();
		}
	}

}
