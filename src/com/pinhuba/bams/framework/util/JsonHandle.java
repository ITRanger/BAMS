package com.pinhuba.bams.framework.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pinhuba.bams.R;
import com.pinhuba.bams.framework.ConfigProperties;

public class JsonHandle {
	private static JsonHandle instance = null;

	public static JsonHandle getInstance() {
		if (instance == null) {
			instance = new JsonHandle();
		}
		return instance;
	}

	public int jsonCodeHandle(JSONObject codeJsonObject) {
		int rst = 0;
		try {
			if (codeJsonObject.isNull("code")) {
				ToastHelper.showShortMsg(R.string.unknow_wrong);
				rst = -1;
			} else {
				rst = codeJsonObject.getInt("code");
				if (rst != 0) {
					if (codeJsonObject.getString("errorMsg").equals(""))
						ToastHelper.showShortMsg(R.string.exception_data_null);
					else {
						try {
							ToastHelper.showShortMsg(new String(codeJsonObject.getString("errorMsg").getBytes(), ConfigProperties.ENCODING));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (JSONException e) {
			ToastHelper.showShortMsg(R.string.unknow_wrong);
			rst = -1;
		}
		Loger.d("rst:" + rst);
		return rst;
	}

	public int jsonCodeHandle(JSONObject codeJsonObject, boolean showErrorMsg) {
		int rst = 0;
		try {
			if (codeJsonObject.isNull("code")) {
				ToastHelper.showShortMsg(R.string.unknow_wrong);
				rst = -1;
			} else {
				rst = codeJsonObject.getInt("code");
				if (rst != 0 && showErrorMsg) {
					if (codeJsonObject.getString("errorMsg").equals(""))
						ToastHelper.showShortMsg(R.string.exception_data_null);
					else {
						try {
							ToastHelper.showShortMsg(new String(codeJsonObject.getString("errorMsg").getBytes(), ConfigProperties.ENCODING));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (JSONException e) {
			if (showErrorMsg)
				ToastHelper.showShortMsg(R.string.unknow_wrong);
			rst = -1;
		}
		return rst;
	}

	public List<HashMap<String, Object>> jsonCodeArrayHandle(JSONArray codeJsonArray, String[] strArray) {

		List<HashMap<String, Object>> tempList = null;
		try {
			tempList = new ArrayList<HashMap<String, Object>>();
			for (int i = 0; i < codeJsonArray.length(); i++) {
				if (!codeJsonArray.isNull(i)) {
					HashMap<String, Object> mMap = new HashMap<String, Object>();
					JSONObject tempObj;
					tempObj = codeJsonArray.getJSONObject(i);
					for (int j = 0; j < strArray.length; j++) {
						if (!tempObj.isNull(strArray[j])) {
							mMap.put(strArray[j], tempObj.getString(strArray[j]));
						} else {
							mMap.put(strArray[j], null);
						}
					}
					tempList.add(mMap);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			tempList = null;
		}
		return tempList;
	}

	/**
	 * 把JsonArray数组转换成<String, Object>的hashMap类型
	 * 
	 * @param needAddList
	 * @param codeJsonArray
	 * @param strArray
	 * @return
	 */
	public List<HashMap<String, Object>> jsonCodeArrayHandle(List<HashMap<String, Object>> needAddList, JSONArray codeJsonArray, String[] strArray) {

		List<HashMap<String, Object>> tempList = null;
		tempList = new ArrayList<HashMap<String, Object>>();
		if (needAddList != null) {
			for (int i = 0; i < needAddList.size(); i++) {
				tempList.add(needAddList.get(i));
			}
		}
		try {
			for (int i = 0; i < codeJsonArray.length(); i++) {
				if (!codeJsonArray.isNull(i)) {// json数组不为空时
					HashMap<String, Object> mMap = new HashMap<String, Object>();
					JSONObject tempObj;
					tempObj = codeJsonArray.getJSONObject(i);
					for (int j = 0; j < strArray.length; j++) {
						if (!tempObj.isNull(strArray[j])) {
							mMap.put(strArray[j], tempObj.getString(strArray[j]));
						} else {
							mMap.put(strArray[j], "");
						}
					}
					tempList.add(mMap);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			tempList = null;
		}
		return tempList;
	}

}
