package com.pinhuba.bams.framework.util;

import com.pinhuba.bams.framework.ConfigProperties;
import com.pinhuba.bams.framework.Constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharePrefsHelper {
	private SharedPreferences sp = null;

	public SharePrefsHelper(Context context) {
		this.sp = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public SharePrefsHelper() {
		this.sp = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
	}

	public SharePrefsHelper(SharedPreferences sp) {
		this.sp = sp;
	}

	public String getLastestUserLoginAccount() {
		if (this.sp != null) {
			return this.sp.getString(Constants.USER_LOGIN_ACCOUNT, "");
		}
		return "";
	}

	public void setLastestUserLoginAccount(String str) {
		if (this.sp != null) {
			this.sp.edit().putString(Constants.USER_LOGIN_ACCOUNT, str).commit();
		}
	}

	public String getLastestUserLoginPwd() {
		if (this.sp != null) {
			return this.sp.getString(Constants.USER_LOGIN_PWD, "");
		}
		return "";
	}

	public void setLastestLoginPwd(String str) {
		if (this.sp != null) {
			this.sp.edit().putString(Constants.USER_LOGIN_PWD, str).commit();
		}
	}

	public String getLastestUserName() {
		if (this.sp != null) {
			return this.sp.getString(Constants.USERNAME, "");
		}
		return "";
	}

	public void setLastestUserName(String str) {
		if (this.sp != null) {
			this.sp.edit().putString(Constants.USERNAME, str).commit();
		}
	}

	public Boolean getIfSavePwd() {
		if (this.sp != null) {
			return this.sp.getBoolean(Constants.SAVE_PWD, false);
		}
		return false;
	}

	public void setIfSavePwd(Boolean value) {
		if (this.sp != null) {
			this.sp.edit().putBoolean(Constants.SAVE_PWD, value).commit();
		}
	}

	public Boolean getIfAutoLogin() {
		if (this.sp != null) {
			return this.sp.getBoolean(Constants.AUTO_LOGIN, false);
		}
		return false;
	}

	public void setIfAutoLogin(Boolean value) {
		if (this.sp != null) {
			this.sp.edit().putBoolean(Constants.AUTO_LOGIN, value).commit();
		}
	}

	public String getSavePath() {
		if (this.sp != null) {
			return this.sp.getString(Constants.SAVEPATH, ConfigProperties.DEFAULT_SAVE_PATH);
		}
		return null;
	}

	public void setSavePath(String str) {
		if (this.sp != null) {
			this.sp.edit().putString(Constants.SAVEPATH, str).commit();
		}
	}

	public String getSessionId() {
		if (this.sp != null) {
			return this.sp.getString(Constants.SESSIONID, null);
		}
		return null;
	}

	public void setSessionId(String str) {
		if (this.sp != null) {
			this.sp.edit().putString(Constants.SESSIONID, str).commit();
		}
	}

	public boolean getIfVoiceOnOff() {
		if (this.sp != null) {
			return this.sp.getBoolean(Constants.VOICEONOFF, true);
		}
		return true;
	}

	public void setIfVoiceOnOff(Boolean value) {
		if (this.sp != null) {
			this.sp.edit().putBoolean(Constants.VOICEONOFF, value).commit();
		}
	}

	public int getTimeRefreshPeriod() {
		if (this.sp != null) {
			return this.sp.getInt(Constants.TIMEREFRESHPERIOD, ConfigProperties.DEFAULT_VOICE_REFRESH_TIME);
		}
		return ConfigProperties.DEFAULT_VOICE_REFRESH_TIME;
	}

	public void setTimeRefreshPeriod(int value) {
		if (this.sp != null) {
			this.sp.edit().putInt(Constants.TIMEREFRESHPERIOD, value).commit();
		}
	}

}
