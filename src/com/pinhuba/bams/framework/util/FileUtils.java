package com.pinhuba.bams.framework.util;

import java.io.File;
import java.io.IOException;

import android.os.Environment;

public class FileUtils {
	private String SDPath;

	public void setSDPath(String SDPath) {
		this.SDPath = SDPath;
	}

	public String getSDPath() {
		return this.SDPath;
	}

	/*
	 * 构造方法    获取SD卡路径    
	 */
	public FileUtils() {
		// 获得当前外部存储设备的目录  
		SDPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
	}

	public File createSDFile(String fileName) throws IOException {
		File file = new File(SDPath + fileName);
		file.createNewFile();
		return file;
	}

	public File createSDDir(String dirName) {
		File dir = new File(SDPath + dirName);
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			dir.mkdir();
		}
		return dir;
	}

	public boolean isSDExist() {
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	}

	public boolean SDMakeDir(String DirName) {
		File file = new File(DirName);
		if (!file.exists()) {
			return file.mkdirs();
		} else {
			return false;
		}
	}

	public boolean isFileExist(String FileName) {
		File file = new File(SDPath + FileName);
		return file.exists();
	}

}
