package com.pinhuba.bams.framework;

public enum UrlType {
	
	LOGINCHECK, // 登录
	
	ANNOUNCE_LIST, // 公告列表
	ANNOUNCE_DETAIL, // 公告内容

}
