package com.pinhuba.bams.framework.controller;

import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pinhuba.bams.framework.UrlType;
import com.pinhuba.bams.framework.util.Loger;

public class Controller {
	private static Controller instance;

	public static Controller getInstance() {
		if (instance == null) {
			instance = new Controller();
		}
		return instance;
	}

	public AsynUpgrade doUpgrade(Context context, ErrorListener listener, String filePath, String apkURL, TextView tv, ProgressBar pb, HashMap<String, String> params) {
		AsynUpgrade task = new AsynUpgrade(context, listener, filePath, apkURL, tv, pb, params);
		task.execute();
		return task;
	}

	public AsynCommon doRequest(Activity context, UrlType type, ErrorListener listener, HashMap<String, String> params) {
		if (type != null) {
			Loger.d(type.name());
		}
		AsynCommon task = new AsynCommon(context, listener, params);
		task.execute(type);
		return task;
	}

}
