package com.pinhuba.bams.framework.controller;

import com.pinhuba.bams.framework.ErrorCode;
import android.widget.ImageView;
import android.widget.TextView;

public interface DocDownloadCallBackListener {
	void docCallBack(int position, int result, ImageView iv, TextView tv, ErrorCode errorcode);
}
