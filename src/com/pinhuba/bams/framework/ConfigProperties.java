package com.pinhuba.bams.framework;

import org.apache.http.protocol.HTTP;
import android.os.Environment;

public class ConfigProperties {

	public static final String COMPANY_CODE = "BIOS";
	
	// Update URL
	public static final String UPDATE_URL = "http://bams.coding.io/update.jsp";

	// Server URL
	public static final String SERVER_URL = "http://bams.coding.io/mobile/";
	
	public static final String ATTACH_DOWNLOAD_URL = "http://bams.coding.io/download.do?fileId=";

	// Network request parameters of the code
	public static final String ENCODING = HTTP.UTF_8;

	// The network connection overtime 10000 ms
	public static final int TIMEOUT = 10000;

	// The ListView default PageSize if The value of PageSize is less or equal 0
	public static final int DefaultPageSize = 10;

	// Debug close
	public static final boolean LOGER_CLOSE = false;

	// Default Save Path
	public static final String DEFAULT_SAVE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/bams/";

	// Default Voice Refresh Time 180 mins
	public static final int DEFAULT_VOICE_REFRESH_TIME = 180;
}
